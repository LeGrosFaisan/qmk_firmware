#include QMK_KEYBOARD_H
#include "version.h"
#include "keymap_mtgap.h"

#define KC_MAC_UNDO LGUI(KC_Z)
#define KC_MAC_CUT LGUI(KC_X)
#define KC_MAC_COPY LGUI(KC_C)
#define KC_MAC_PASTE LGUI(KC_V)
#define KC_PC_UNDO LCTL(KC_Z)
#define KC_PC_CUT LCTL(KC_X)
#define KC_PC_COPY LCTL(KC_C)
#define KC_PC_PASTE LCTL(KC_V)
#define ES_LESS_MAC KC_GRAVE
#define ES_GRTR_MAC LSFT(KC_GRAVE)
#define ES_BSLS_MAC ALGR(KC_6)
#define NO_PIPE_ALT KC_GRAVE
#define NO_BSLS_ALT KC_EQUAL
#define LSA_T(kc) MT(MOD_LSFT | MOD_LALT, kc)
#define BP_NDSH_MAC ALGR(KC_8)
#define SE_SECT_MAC ALGR(KC_6)
#define MOON_LED_LEVEL LED_LEVEL

enum layers {
    BASE,  // default layer
    SYMB,  // symbols
    MDIA,  // media keys
};

enum custom_keycodes {
  RGB_SLD = ML_SAFE_RANGE,
  HSV_0_255_255,
  HSV_86_255_128,
  HSV_172_255_255,
  BP_LSPO,
  BP_RSPC,
};


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    /*
     * ┌──────┬───┬───┬───┬───┬───┬───┐                     ┌───┬───┬───┬───┬───┬───┬──────┐
     * │ `    │ ^ │ * │ = │ ( │ ! │ & │                     │ $ │ # │ ) │ _ │ @ │ \ │    % │
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │ [    │ Y │ P │ O │ U │ I │ L1│                     │ L2│ K │ D │ L │ C │ W │    ] │
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │Bspace│ I │ N │ E │ A │ , │Tab│                     │Del│ M │ H │ T │ S │ R │    ' │
     * ├──────┼───┼───┼───┼───┼───┼───┘                     └───┼───┼───┼───┼───┼───┼──────┤
     * │Insert│ Q │ Z │ ´ │ . │ / │                             │ B │ F │ G │ V │ X │ Shift│
     * └──┬───┼───┼───┼───┼───┼───┘     ┌───────┐ ┌───────┐     └───┼───┼───┼───┼───┼───┬──┘
     *    │L1 │CAP│HYP│ ← │ → │         │ Alt   │ │Escape │         │ ↑ │ ↓ │MEH│Ent│L2 │
     *    └───┴───┴───┴───┴───┘     ┌───┼───────┤ ├───────┼───┐     └───┴───┴───┴───┴───┘
     *                              │   │   │   │ │   │   │   │
     *                              │Sp │Sft│Win│ │Ctr│AGr│Bsp│
     *                              │   │   │   │ │   │   │   │
     *                              └───┴───┴───┘ └───┴───┴───┘
     */
    [BASE] = LAYOUT_moonlander(
        MT_GRV,         MT_CIRC,        MT_ASTR,        MT_EQL,         MT_LPRN,        MT_EXLM,        MT_APRS,                                        MT_DLR,         MT_HASH,        MT_RPRN,        MT_UNDS,        MT_AT,          MT_BSLS,        MT_PERC,
        MT_LBRC,        MT_Y,           MT_P,           MT_O,           MT_U,           MT_J,           TG(1),                                          TG(2),          MT_K,           MT_D,           MT_L,           MT_C,           MT_W,           MT_RBRC,
        KC_BSPC,        MT_I,           MT_N,           MT_E,           MT_A,           MT_COMM,        KC_TAB,                                         KC_DELETE,      MT_M,           MT_H,           MT_T,           MT_S,           MT_R,           MT_QUOT,
        KC_INS,         LCTL_T(MT_Q),   MT_Z,           MT_ACUT,        MT_DOT,         MT_SLSH,                                                                        MT_B,           MT_F,           MT_G,           MT_V,           RCTL_T(MT_X),   KC_RSFT,
        MO(1),          CW_TOGG,        KC_HYPR,        KC_LEFT,        KC_RIGHT,   LALT_T(KC_APPLICATION),                                                             LCTL_T(KC_ESC), KC_UP,          KC_DOWN,        KC_MEH,         KC_ENTER,       MO(2),
        KC_SPACE,       KC_LSFT,        KC_LGUI,                        KC_LCTL,        KC_RALT,        KC_BSPC
    ),
        /*
     * ┌──────┬───┬───┬───┬───┬───┬───┐                     ┌───┬───┬───┬───┬───┬───┬──────┐
     * │ Esc  │ F1│ F2│ F3│ F4│ F5│   │                     │   │ F6│ F7│ F8│ F9│F10│  F11 │
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │ [    │   │   │   │   │   │ L1│                     │ L2│   │ 7 │ 8 │ 9 │ / │  F12 │
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │Bspace│   │   │   │   │   │Tab│                     │Del│   │ 4 │ 5 │ 6 │ * │      │
     * ├──────┼───┼───┼───┼───┼───┼───┘                     └───┼───┼───┼───┼───┼───┼──────┤
     * │Insert│CTL│ALT│   │   │   │                             │ 0 │ 1 │ 2 │ 3 │ - │ Shift│
     * └──┬───┼───┼───┼───┼───┼───┘     ┌───────┐ ┌───────┐     └───┼───┼───┼───┼───┼───┬──┘
     *    │L1 │...│RED│HOM│END│         │RGB_MOD│ │RGB_TG │         │PUP│PDN│ . │ + │L2 │
     *    └───┴───┴───┴───┴───┘     ┌───┼───────┤ ├───────┼───┐     └───┴───┴───┴───┴───┘
     *                              │RGB│RGB│RGB│ │RGB│RGB│RGB│
     *                              │VAD│VAI│COL│ │SLD│HUD│HUI│
     *                              │   │   │   │ │   │   │   │
     *                              └───┴───┴───┘ └───┴───┴───┘
     */
    [SYMB] = LAYOUT_moonlander(
        KC_ESCAPE,      KC_F1,          KC_F2,          KC_F3,          KC_F4,          KC_F5,          KC_NO         ,                                 KC_NO,          KC_F6,          KC_F7,          KC_F8,          KC_F9,          KC_F10,         KC_F11,
        KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          MT_7,           MT_8,           MT_9,           MT_SLSH,        KC_F12,
        KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          MT_4,           MT_5,           MT_6,           MT_ASTR,        KC_NO,
        KC_TRANSPARENT, KC_LCTL,        KC_LALT,        KC_NO,          KC_NO,          KC_NO,                                                                          MT_0,           MT_1,           MT_2,           MT_3,           MT_MINU,        KC_TRANSPARENT,
        KC_TRANSPARENT, KC_NO,          HSV_0_255_255,  KC_HOME,        KC_END,         RGB_MOD,                                                                        RGB_TOG,        KC_PGUP,        KC_PGDN,        MT_DOT,         MT_PLUS,        KC_TRANSPARENT,
        RGB_VAD,        RGB_VAI,        TOGGLE_LAYER_COLOR,             RGB_SLD,        RGB_HUD,        RGB_HUI
    ),
            /*
     * ┌──────┬───┬───┬───┬───┬───┬───┐                     ┌───┬───┬───┬───┬───┬───┬──────┐
     * │ Esc  │   │   │   │   │   │   │                     │   │   │   │   │   │   │ RESET│
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │ [    │   │   │ ↑ │   │   │ L1│                     │ L2│   │   │   │   │   │      │
     * ├──────┼───┼───┼───┼───┼───┼───┤                     ├───┼───┼───┼───┼───┼───┼──────┤
     * │Bspace│   │ ← │ ↓ │ → │   │Tab│                     │Del│   │   │   │   │   │      │
     * ├──────┼───┼───┼───┼───┼───┼───┘                     └───┼───┼───┼───┼───┼───┼──────┤
     * │Insert│   │   │   │   │   │                             │   │   │   │   │   │ Shift│
     * └──┬───┼───┼───┼───┼───┼───┘     ┌───────┐ ┌───────┐     └───┼───┼───┼───┼───┼───┬──┘
     *    │L1 │   │WRF│W← │W→ │         │       │ │       │         │   │M← │M→ │MPL│L2 │
     *    └───┴───┴───┴───┴───┘     ┌───┼───────┤ ├───────┼───┐     └───┴───┴───┴───┴───┘
     *                              │MS │MS │MS │ │Vol│Vol│Vol│
     *                              │BTN│BTN│BTN│ │Off│ ↑ │ ↓ │
     *                              │ 1 │ 2 │ 3 │ │   │   │   │
     *                              └───┴───┴───┘ └───┴───┴───┘
     */
    [MDIA] = LAYOUT_moonlander(
        KC_ESCAPE,      KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,                                          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          RESET,
        KC_TRANSPARENT, KC_NO,          KC_NO,          KC_MS_UP,       KC_NO,          KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,
        KC_TRANSPARENT, KC_NO,          KC_MS_LEFT,     KC_MS_DOWN,     KC_MS_RIGHT,    KC_NO,          KC_TRANSPARENT,                                 KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,
        KC_TRANSPARENT, KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,                                                                          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_NO,          KC_TRANSPARENT,
        KC_TRANSPARENT, KC_NO,          KC_WREF,        KC_WBAK,        KC_WFWD,        KC_NO,                                                                          KC_NO,          KC_MPRV,        KC_MNXT,        KC_MPLY,        KC_NO,          KC_TRANSPARENT,
        KC_MS_BTN1,     KC_MS_BTN2,     KC_MS_BTN3,                     KC_MUTE,        KC_VOLU,        KC_VOLD
  ),
};


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
    case RGB_SLD:
        if (record->event.pressed) {
            rgblight_mode(1);
        }
        return false;
    case HSV_0_255_255:
        if (record->event.pressed) {
            rgblight_mode(1);
            rgblight_sethsv(0,255,255);
        }
        return false;
    case HSV_86_255_128:
        if (record->event.pressed) {
            rgblight_mode(1);
            rgblight_sethsv(86,255,128);
        }
        return false;
    case HSV_172_255_255:
        if (record->event.pressed) {
            rgblight_mode(1);
            rgblight_sethsv(172,255,255);
        }
        return false;
    }
    return true;
}
