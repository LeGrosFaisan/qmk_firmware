/* Copyright 2022
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "keycodes.h"

// clang-format off

/*
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │ ` │ ^ │ * │ = │ ( │ ! │ # │ ) │ _ │ @ │ \ │ & │ $ │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │ Y │ P │ O │ U │ J │ K │ D │ L │ C │ W │ [ │ ] │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │ I │ N │ E │ A │ , │ M │ H │ T │ S │ R │ ' │ % │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │   │ Q │ Z │ ´ │ . │ / │ B │ F │ G │ V │ X │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 */
// Row 1
#define MT_GRV  KC_GRV  // `
#define MT_CIRC KC_1    // ^
#define MT_ASTR KC_2    // *
#define MT_EQL  KC_3    // =
#define MT_LPRN KC_4    // (
#define MT_EXLM KC_5    // !
#define MT_HASH KC_6    // #
#define MT_RPRN KC_7    // )
#define MT_UNDS KC_8    // _
#define MT_AT   KC_9    // @
#define MT_BSLS KC_0    // Backslash
#define MT_APRS KC_MINS // &
#define MT_DLR  KC_EQL  // $
// Row 2
#define MT_Y    KC_Q    // Y
#define MT_P    KC_W    // P
#define MT_O    KC_E    // O
#define MT_U    KC_R    // U
#define MT_J    KC_T    // J
#define MT_K    KC_Y    // K
#define MT_D    KC_U    // D
#define MT_L    KC_I    // L
#define MT_C    KC_O    // C
#define MT_W    KC_P    // W
#define MT_LBRC KC_LBRC // [
#define MT_RBRC KC_RBRC // ]
// Row 3
#define MT_I    KC_A    // I
#define MT_N    KC_S    // N
#define MT_E    KC_D    // E
#define MT_A    KC_F    // A
#define MT_COMM KC_G    // ,
#define MT_M    KC_H    // M
#define MT_H    KC_J    // H
#define MT_T    KC_K    // T
#define MT_S    KC_L    // S
#define MT_R    KC_SCLN // R
#define MT_QUOT KC_QUOT // '
#define MT_PERC KC_NUHS // %
// Row 4
#define MT_ KC_NUBS //
#define MT_Q    KC_Z    // Q
#define MT_Z    KC_X    // Z
#define MT_ACUT KC_C    // ´
#define MT_DOT  KC_V    // .
#define MT_SLSH KC_B    // Slash
#define MT_B    KC_N    // B
#define MT_F    KC_M    // F
#define MT_G    KC_COMM // G
#define MT_V    KC_DOT  // V
#define MT_X    KC_SLSH // X

/* Shifted symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │ ~ │ 8 │ 9 │ 2 │ 3 │ 4 │ 5 │ 1 │ 0 │ 6 │ 7 │   │   │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │   │   │   │   │   │   │   │   │   │   │ { │ } │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │   │   │   │   │ ; │   │   │   │   │   │ " │ %%│    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │   │   │   │ ` │ : │ ? │   │   │   │   │   │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 */
// Row 1
#define MT_TLDE S(MT_GRV)  // ~
#define MT_8    S(MT_CIRC) // 8
#define MT_9    S(MT_ASTR) // 9
#define MT_2    S(MT_EQL)  // 2
#define MT_3    S(MT_LPRN) // 3
#define MT_4    S(MT_EXLM) // 4
#define MT_5    S(MT_HASH) // 5
#define MT_1    S(MT_RPRN) // 1
#define MT_0    S(MT_UNDS) // 0
#define MT_6    S(MT_AT)   // 6
#define MT_7    S(MT_BSLS) // 7
// Row 2
#define MT_LCRB S(MT_LBRC) // {
#define MT_RCRB S(MT_RBRC) // }
// Row 3
#define MT_SCLN S(MT_COMM) // ;
#define MT_DQUO S(MT_QUOT) // "
// Row 4
#define MT_DGRV  S(MT_ACUT) // ` (dead)
#define MT_COLN S(MT_DOT)  // :
#define MT_QUES S(MT_SLSH) // ?

/* AltGr symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │   │   │ !=│ + │ < │ ¡ │   │ > │ - │   │ | │   │   │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │ ¥ │ £ │ Œ │   │   │ © │ ° │   │   │   │ “ │ ” │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │   │   │ € │ Æ │   │ μ │ ± │ ™ │ ß │ ® │   │   │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │   │ Ω │   │ ^ │...│ ¿ │   │   │   │   │   │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 */
// Row 1
#define MT_NOT  ALGR(MT_ASTR) // !=
#define MT_PLUS ALGR(MT_EQL)  // +
#define MT_LESS ALGR(MT_LPRN) // <
#define MT_IEXL ALGR(MT_EXLM) // ¡
#define MT_GRTR ALGR(MT_RPRN) // >
#define MT_MINU ALGR(MT_UNDS) // -
#define MT_PIPE ALGR(MT_BSLS) // |
// Row 2
#define MT_YEN  ALGR(MT_Y)    // ¥
#define MT_PND  ALGR(MT_P)    // £
#define MT_OE   ALGR(MT_O)    // Œ
#define MT_COPY ALGR(MT_K)    // ©
#define MT_DEG  ALGR(MT_D)    // °
#define MT_LDQU ALGR(MT_LBRC) // “
#define MT_RDQU ALGR(MT_RBRC) // ”
// Row 3
#define MT_EURO ALGR(MT_E)    // €
#define MT_AE   ALGR(MT_A)    // Æ
#define MT_MICR ALGR(MT_M)    // μ
#define MT_PLMN ALGR(MT_H)    // ±
#define MT_TM   ALGR(MT_T)    // ™
#define MT_SS   ALGR(MT_S)    // ß
#define MT_REGD ALGR(MT_R)    // ®
// Row 4
#define MT_OMEG ALGR(MT_Q)    // Ω
#define MT_DCIR ALGR(MT_ACUT) // ^ (dead)
#define MT_ELLI ALGR(MT_DOT)  // ellipsis
#define MT_IQUE ALGR(MT_SLSH) // ¿

/* Right Ctrl symbols
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐
 * │   │   │   │ ² │ « │   │   │ » │-- │   │---│   │   │       │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤
 * │     │   │   │   │   │   │   │   │   │   │   │   │   │     │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┐    │
 * │      │   │   │   │   │   │   │   │   │   │   │   │   │    │
 * ├────┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴───┴────┤
 * │    │   │   │   │ ¨ │   │   │   │   │   │   │   │          │
 * ├────┼───┴┬──┴─┬─┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤
 * │    │    │    │                        │    │    │    │    │
 * └────┴────┴────┴────────────────────────┴────┴────┴────┴────┘
 */
// Row 1
#define MT_SUP2 RCTL(MT_EQL)  // ²
#define MT_RGUI RCTL(MT_LPRN) // «
#define MT_LGUI RCTL(MT_RPRN) // »
#define MT_ENDH RCTL(MT_UNDS) // --
#define MT_EMDH RCTL(MT_BSLS) // ---

// Row 4
#define MT_DIAE RCTL(MT_ACUT) // ¨ (dead)
